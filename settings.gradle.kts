pluginManagement {
    repositories {
        gradlePluginPortal()
    }
}
rootProject.name = "lab-inventory"
include("backend")
include("frontend")
