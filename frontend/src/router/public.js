// Public routes that do not require an authenticated login
// See examples below:

// import Login from '@/views/Login.vue'
// import Registration from '@/views/Registration.vue'
// import ForgotPassword from '@/views/ForgotPassword.vue'
import Home from "@/views/Home.vue";

const routes = [
  // {
  //     path: '/login',
  //     name: 'login',
  //     component: Login
  // },
  // {
  //     path: '/registration',
  //     name: 'registration',
  //     component: Registration
  // },
  // {
  //     path: '/forgot-password',
  //     name: 'forgotPassword',
  //     component: ForgotPassword
  // }
  {
    path: "/home",
    name: "home",
    component: Home
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  }
];

export default routes.map(route => {
  const meta = {
    public: true,
    onlyLoggedOut: true
  };
  return { ...route, meta };
});
