import Vue from "vue";
import Router from "vue-router";
import routes from "@/router/index.js";
// TODO for future Vuejs store support needed for authentication and other data
// import store from '@/store/index.js'

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      // TODO update this to a default or dashboard page as needed
      redirect: "/home"
    }
  ].concat(routes)
});

// TODO to be executed before each route when authentication is implemented
// router.beforeEach((to, from, next) => {
//   const authenticated = store.state.user.authenticated
//   const onlyLoggedOut = to.matched.some(record => record.meta.onlyLoggedOut)
//   const isPublic = to.matched.some(record => record.meta.public)
//   if (!isPublic && !authenticated) {
//     // this route requires auth, check if logged in
//     // if not, redirect to login page.
//     return next({
//       path: '/login',
//       query: { redirect: to.fullPath }
//     })
//   }
//   if (authenticated && onlyLoggedOut) {
//     return next('/')
//   }
//   next()
// })

export default router;
