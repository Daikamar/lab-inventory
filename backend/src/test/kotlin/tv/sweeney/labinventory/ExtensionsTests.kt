package tv.sweeney.labinventory

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import java.time.LocalDateTime

@Tag("UnitTest")
class ExtensionsTests {

    @Test
    fun `Assert proper date formatting`() {
        val time: LocalDateTime = LocalDateTime.of(2020, 1, 1, 1, 1, 1)
        assertThat(time.format()).isEqualTo("2020-01-01 1st 2020")
    }

    @Test
    fun `Assert proper ordinal returned`() {
        val first: String = getOrdinal(1)
        val second: String = getOrdinal(2)
        val third: String = getOrdinal(3)
        val fourth: String = getOrdinal(4)
        val nth: String = getOrdinal(36)
        assertThat(first).isEqualTo("1st")
        assertThat(second).isEqualTo("2nd")
        assertThat(third).isEqualTo("3rd")
        assertThat(fourth).isEqualTo("4th")
        assertThat(nth).isEqualTo("36th")
    }
}
