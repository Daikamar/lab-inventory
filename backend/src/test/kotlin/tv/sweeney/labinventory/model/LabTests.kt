package tv.sweeney.labinventory.model

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test

@Tag("UnitTest")
class LabTests {

    @Test
    fun `Create Lab`() {
        val lab = Lab(
            "Lab-001",
            "USA",
            "PA",
            "Philadelphia",
            "Building 1"
        )
        assertThat(lab.name).isEqualTo("Lab-001")
        assertThat(lab.country).isEqualTo("USA")
        assertThat(lab.region).isEqualTo("PA")
        assertThat(lab.city).isEqualTo("Philadelphia")
        assertThat(lab.location).isEqualTo("Building 1")
    }

}
