package tv.sweeney.labinventory.model

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test

@Tag("UnitTest")
class VirtualHostTests {

    @Test
    fun `Create VirtualHost`() {
        val lab = Lab("Lab-001", "USA", "PA", "Philadelphia", "Building 1")
        val rack = Rack("Rack-001", 48, lab)
        val virtualHost = VirtualHost("VHost-001", NodeType.VIRTUAL_HOST, rack, 2, 20)
        Assertions.assertThat(virtualHost.name).isEqualTo("VHost-001")
        Assertions.assertThat(virtualHost.nodeType).isEqualTo(NodeType.VIRTUAL_HOST)
        Assertions.assertThat(virtualHost.size).isEqualTo(2)
        Assertions.assertThat(virtualHost.rackPosition).isEqualTo(20)
        Assertions.assertThat(virtualHost.rack).isEqualTo(rack)
    }
}
