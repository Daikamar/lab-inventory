package tv.sweeney.labinventory.util

import java.net.InetAddress

// TODO need to keep an eye on this and see if it makes sense to refactor these to use strictly Int

/**
 * Converts a String IPv4 address to a UInt (unsigned integer)
 */
fun ipStringToInt(address: String): Int {
    var ipAsInt: UInt = 0u
    var bytePosition = 16777216u
    for (currentByte in InetAddress.getByName(address).address) {
        ipAsInt += currentByte.toUByte() * bytePosition
        bytePosition /= 256u
    }
    return ipAsInt.toInt()
}

/**
 * Converts a UInt IPv4 address to a String
 */
fun ipIntToString(address: Int): String {
    var bytePosition = 16777216u
    var byteMax = 4278190080u
    val ipAsByteArray = ByteArray(4)
    for (i in 0..3) {
        ipAsByteArray[i] = ((address.toUInt() and byteMax)/bytePosition).toByte()
        byteMax /= 256u
        bytePosition /= 256u
    }
    return InetAddress.getByAddress(ipAsByteArray).hostAddress
}
