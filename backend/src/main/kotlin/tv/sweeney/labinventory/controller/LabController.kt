package tv.sweeney.labinventory.controller

import tv.sweeney.labinventory.model.Lab
import tv.sweeney.labinventory.repository.LabRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody

@RestController
@RequestMapping("/api/labs")
class LabController(private val repository: LabRepository) {

    @GetMapping("/")
    fun getAllNetworks() = repository.findAllByOrderByNameAsc()

    @GetMapping("/{id}")
    fun getNetwork(@PathVariable id: Long) = repository.findById(id)

    @PostMapping("/create")
    fun createNetwork(@RequestBody lab: Lab) = repository.save(lab)

    @PutMapping("/update")
    fun updateNetwork(@RequestBody lab: Lab) = repository.save(lab)
}
