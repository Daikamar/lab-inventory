package tv.sweeney.labinventory.controller

import tv.sweeney.labinventory.model.Network
import tv.sweeney.labinventory.model.NetworkDto
import tv.sweeney.labinventory.repository.NetworkRepository
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody

@RestController
@RequestMapping("/api/networks")
class NetworkController(private val repository: NetworkRepository) {

    @GetMapping("/")
    fun getAllNetworks() = repository.findAllByOrderByNameAsc()

    @GetMapping("/{id}")
    fun getNetwork(@PathVariable id: Long) = repository.findById(id)

    @PostMapping("/create")
    fun createNetwork(@RequestBody networkDto: NetworkDto): Network {
        val network = Network(networkDto.name, networkDto.baseAddress, networkDto.subnetAddress, networkDto.gatewayAddress, networkDto.broadcastAddress)
        return repository.save(network)
    }

    @PutMapping("/update")
    fun updateNetwork(@RequestBody network: Network) = repository.save(network)
}

