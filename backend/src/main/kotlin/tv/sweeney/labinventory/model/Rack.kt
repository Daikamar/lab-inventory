package tv.sweeney.labinventory.model

import com.fasterxml.jackson.annotation.JsonManagedReference
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne
import javax.persistence.OneToMany

@Entity
class Rack(
    val name: String,
    val size: Int,
    @ManyToOne val lab: Lab,
    @Id @GeneratedValue val id: Long? = null
) {
    @OneToMany(mappedBy = "rack", fetch = FetchType.EAGER, cascade = [CascadeType.ALL])
    @JsonManagedReference
    val nodes: List<Node>? = emptyList()
    /**
     * Checks for available positions in the rack
     *
     * @return a list of indices for available rack units
     */
//    fun openPositions() : List<Int> {
//        val open: MutableList<Int> = (1..size).toMutableList()
//
//        for (node in nodes) {
//            //takes size into account for open positions
//            if (node is RackNode) {
//                for(i in 0 until node.size) {
//                    open.remove(node.rackPosition + i)
//                }
//            }
//        }
//        return open
//    }

    /**
     * Modifies size of the rack, checks to make sure
     * there's enough room to modify the rack size and that
     * no nodes will get truncated
     *
     * @param newSize the new size of the rack
     * @return if the size can be adjusted successfully
     */
//    fun changeSize(newSize : Int) : Boolean {
//        //if new size is smaller, check if there are any nodes at the end
//        if(newSize < size) {
//            for (node in nodes) {
//                for(i in 0 until node.size) {
//                    if(node.rackPosition + i + 1 > newSize) {
//                        return false
//                    }
//                }
//            }
//
//        }
//        size = newSize
//        return true
//    }

    /**
     * Checks to see if a node of a certain size can fit
     * at a given position
     *
     * @param size
     * @param position
     * @return whether the node can fit or not
     */
//    fun nodeFits(size: Int, position: Int) : Boolean {
//        val open = openPositions()
//
//        for(i in 0 until size) {
//            if(position + i !in open) {
//                return false
//            }
//        }
//        return true
//    }
}
