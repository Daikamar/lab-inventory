package tv.sweeney.labinventory.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
class Lab(
        val name: String,
        val country: String,
        val region: String,
        val city: String,
        val location: String,
        @Id @GeneratedValue val id: Long? = null
)

