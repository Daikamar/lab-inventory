package tv.sweeney.labinventory.model

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import javax.persistence.CascadeType
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "nodeType", visible = true)
@JsonSubTypes(
    JsonSubTypes.Type(value = VirtualHost::class, name = "VIRTUAL_HOST")
)
@Entity
class Node(
    val name: String,
    val nodeType: NodeType,
    @ManyToOne(fetch = FetchType.EAGER, cascade = [CascadeType.MERGE])
    @JsonBackReference
    val rack: Rack? = null,
    @Id @GeneratedValue var id: Long? = null
)

enum class NodeType {
    VIRTUAL_HOST
}
