package tv.sweeney.labinventory.model

import javax.persistence.Entity

@Entity
class VirtualHost(
    name: String,
    nodeType: NodeType,
    rack: Rack,
    override val size: Int,
    override val rackPosition: Int
) : Node(name, nodeType, rack), RackNode
